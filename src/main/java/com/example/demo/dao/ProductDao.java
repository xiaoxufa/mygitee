package com.example.demo.dao;

import com.example.demo.entity.ProductEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author xxf
 * @email 908754244@qq.com
 * @date 2021-01-15 15:02:44
 */
@Mapper
public interface ProductDao extends BaseMapper<ProductEntity> {

    int deductProductStock(@Param("productId") Long productId);

}
