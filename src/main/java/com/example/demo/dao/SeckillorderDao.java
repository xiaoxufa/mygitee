package com.example.demo.dao;

import com.example.demo.entity.SeckillorderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author xxf
 * @email 908754244@qq.com
 * @date 2021-01-15 15:02:44
 */
@Mapper
public interface SeckillorderDao extends BaseMapper<SeckillorderEntity> {
	
}
