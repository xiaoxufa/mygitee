package com.example.demo.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.example.demo.common.Constants;
import com.example.demo.common.utils.Constant;
import com.example.demo.common.utils.PageUtils;
import com.example.demo.common.utils.R;
import com.example.demo.entity.SeckillorderEntity;
import com.example.demo.service.SeckillorderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import com.example.demo.entity.ProductEntity;
import com.example.demo.service.ProductService;

import javax.annotation.PostConstruct;


/**
 * @author xxf
 * @email 908754244@qq.com
 * @date 2021-01-15 15:02:44
 */
@Slf4j
@RestController
@RequestMapping("demo/product")
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private SeckillorderService seckillorderService;
    @Autowired
    private ZooKeeper zooKeeper;
    private static ConcurrentHashMap<Long, Object> productSoldOutMap = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        List<ProductEntity> list = productService.list();
        for (ProductEntity p : list) {
            stringRedisTemplate.opsForValue().set(Constants.REDIS_PRODUCT_STOCK_PREFIX + p.getId(), p.getStock() + "");
        }
    }

    public static ConcurrentHashMap<Long, Object> getProductSoldOutMap() {
        return productSoldOutMap;
    }

    @PostMapping("/{productId}")
    public R seckill(@PathVariable("productId") Long productId) throws KeeperException, InterruptedException {
        if (productSoldOutMap.get(productId)!=null){
            R.error(3,"商品已售完");
        }
        Long stock = stringRedisTemplate.opsForValue().decrement(Constants.REDIS_PRODUCT_STOCK_PREFIX + productId);
        if(stock<0){
            //内存变量记录是否卖完
            productSoldOutMap.put(productId,true);
            log.info("======设置商品{}售完标记====",productId);
            //防止报错后有库存了少买
            stock = stringRedisTemplate.opsForValue().increment(Constants.REDIS_PRODUCT_STOCK_PREFIX + productId);
            log.info("====================stock:{}",stock);

            String zkSoldOutProductPath = Constants.getZKSoldOutProductPath(productId);
            if(zooKeeper.exists(zkSoldOutProductPath,true)==null){
                zooKeeper.create(zkSoldOutProductPath, "true".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);

            }
            //监听zk售完标记节点
            zooKeeper.exists(zkSoldOutProductPath,true);
            return   R.error(3,"商品已售完");
        }

        try {
            seckillorderService.seckill(productId);
        } catch (Exception e) {
            //报错redis库存数量还原加一
            stringRedisTemplate.opsForValue().increment(Constants.REDIS_PRODUCT_STOCK_PREFIX + productId);
            if(productSoldOutMap.get(productId)!=null){
                productSoldOutMap.remove(productId);
            }
            //修改zk商品售完标记为false
            if(zooKeeper.exists(Constants.getZKSoldOutProductPath(productId),true)!=null){
                zooKeeper.setData(Constants.getZKSoldOutProductPath(productId), "false".getBytes(), -1);

            }
            log.error("创建订单失败", e);
            return R.error(2, "创建订单失败");
        }


        return R.ok();
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("demo:product:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = productService.queryPage(params);
        Long productId = 0l;
        productService.deductProductStock(productId);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("demo:product:info")
    public R info(@PathVariable("id") Integer id) {
        ProductEntity product = productService.getById(id);

        return R.ok().put("product", product);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("demo:product:save")
    public R save(@RequestBody ProductEntity product) {
        productService.save(product);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("demo:product:update")
    public R update(@RequestBody ProductEntity product) {
        productService.updateById(product);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("demo:product:delete")
    public R delete(@RequestBody Integer[] ids) {
        productService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
