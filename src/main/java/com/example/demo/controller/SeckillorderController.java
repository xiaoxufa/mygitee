package com.example.demo.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.example.demo.common.utils.PageUtils;
import com.example.demo.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import com.example.demo.entity.SeckillorderEntity;
import com.example.demo.service.SeckillorderService;

import javax.annotation.PostConstruct;


/**
 * @author xxf
 * @email 908754244@qq.com
 * @date 2021-01-15 15:02:44
 */
@Slf4j
@RestController
@RequestMapping("demo/seckillorder")
public class SeckillorderController {
    @Autowired
    private SeckillorderService seckillorderService;


    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("demo:seckillorder:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = seckillorderService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("demo:seckillorder:info")
    public R info(@PathVariable("id") Integer id) {
        SeckillorderEntity seckillorder = seckillorderService.getById(id);

        return R.ok().put("seckillorder", seckillorder);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("demo:seckillorder:save")
    public R save(@RequestBody SeckillorderEntity seckillorder) {
        seckillorderService.save(seckillorder);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("demo:seckillorder:update")
    public R update(@RequestBody SeckillorderEntity seckillorder) {
        seckillorderService.updateById(seckillorder);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("demo:seckillorder:delete")
    public R delete(@RequestBody Integer[] ids) {
        seckillorderService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
