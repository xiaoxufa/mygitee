package com.example.demo.service.impl;

import com.example.demo.common.utils.PageUtils;
import com.example.demo.common.utils.Query;
import com.example.demo.entity.ProductEntity;
import com.example.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.example.demo.dao.SeckillorderDao;
import com.example.demo.entity.SeckillorderEntity;
import com.example.demo.service.SeckillorderService;
import org.springframework.transaction.annotation.Transactional;


@Service("seckillorderService")
public class SeckillorderServiceImpl extends ServiceImpl<SeckillorderDao, SeckillorderEntity> implements SeckillorderService {

    @Autowired
    ProductService productService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SeckillorderEntity> page = this.page(
                new Query<SeckillorderEntity>().getPage(params),
                new QueryWrapper<SeckillorderEntity>()
        );

        return new PageUtils(page);
    }

    @Transactional
    public void seckill(Long productId) {
        //查询商品
        ProductEntity productEntity = productService.getById(productId);
        if (productEntity.getStock() <=0) {
            throw new RuntimeException("商品库存已售完");
        }
        //创建秒杀订单
        SeckillorderEntity order = new SeckillorderEntity();
        order.setProductid(productId);
        order.setAmount(productEntity.getPrice());
        this.save(order);

        //减库存
        Integer updateNum = productService.deductProductStock(productId);
        if(updateNum<=0){
            throw new RuntimeException("商品库存已售完");
        }
    }
}