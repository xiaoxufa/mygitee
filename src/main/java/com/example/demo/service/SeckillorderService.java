package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.example.demo.common.utils.PageUtils;
import com.example.demo.entity.SeckillorderEntity;

import java.util.Map;

/**
 * 
 *
 * @author xxf
 * @email 908754244@qq.com
 * @date 2021-01-15 15:02:44
 */
public interface SeckillorderService extends IService<SeckillorderEntity> {

    PageUtils queryPage(Map<String, Object> params);


    void seckill(Long productId);
}

