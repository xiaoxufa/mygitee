package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author xxf
 * @email 908754244@qq.com
 * @date 2021-01-15 15:02:44
 */
@Data
@TableName("seckillorder")
public class SeckillorderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 
	 */
	private Long productid;
	/**
	 * 
	 */
	private BigDecimal amount;

}
