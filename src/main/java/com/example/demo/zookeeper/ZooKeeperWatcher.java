package com.example.demo.zookeeper;

import com.example.demo.common.Constants;
import com.example.demo.common.utils.SpringContextUtils;
import com.example.demo.controller.ProductController;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ZooKeeperWatcher implements Watcher {
    private ZooKeeper zooKeeper;
    private ApplicationContext applicationContext =SpringContextUtils.getApplicationContext();


    @Override
    public void process(WatchedEvent event) {
        if (event.getType() == Event.EventType.None && event.getPath() == null) {
            log.info("=============ZooKeeper连接成功================");
            if (zooKeeper == null) {
                zooKeeper =(ZooKeeper) this.applicationContext.getBean(ZooKeeper.class);
            }
            //创建zk的商品售完标记根节点
            try {
                if (zooKeeper.exists(Constants.ZK_PRODUCT_SOLD_FLAG, false) == null) {
                    zooKeeper.create(Constants.ZK_PRODUCT_SOLD_FLAG, "".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (event.getType() == Event.EventType.NodeDataChanged) {
            //zk目录节点数据变化通知事件
            try {
                String path = event.getPath();
                String soldOutFlag = new String(zooKeeper.getData(path, true, new Stat()));
                log.info("zookeeper数据节点修改变动，path={}，value={}", path, soldOutFlag);
                if ("false".equalsIgnoreCase(soldOutFlag)) {
                    String productId = path.substring(path.lastIndexOf("/") + 1, path.length());
                    ProductController.getProductSoldOutMap().remove(productId);
                }
            } catch (Exception e) {
                log.error("zookeeper数据节点修改回调异常", e);
            }
        }

    }
}
